package com.example.optionalservice;

/**
 * Accessor interface to safely get a {@link Foo} instance if one is available.
 */
public interface FooAccessor
{
    Foo getFoo();
}
