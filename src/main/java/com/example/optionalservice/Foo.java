package com.example.optionalservice;

import com.pyxis.greenhopper.GreenHopper;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A very simple class composed of a GreenHopper component.
 */
public class Foo
{
    private final GreenHopper greenhopper;

    public Foo(GreenHopper greenhopper)
    {
        this.greenhopper = checkNotNull(greenhopper, "greenhopper");
    }
    
    public Object getGreenHopperConfig()
    {
        return greenhopper.getGHConfiguration();
    }
}
