package com.example.optionalservice;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Simple servlet to test whether or not the optional component (composed of GreenHopper components) is accessible.
 */
public class MyServlet extends HttpServlet
{
    private final FooAccessor fooAccessor;

    public MyServlet(FooAccessor fooAccessor)
    {
        this.fooAccessor = checkNotNull(fooAccessor, "fooAccessor");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        Boolean exists = fooAccessor.getFoo() != null;
        resp.getWriter().write("Foo (composed of a GreenHopper component) exists: " + exists);
        if (exists)
        {
            resp.getWriter().write("<br>");
            resp.getWriter().write(fooAccessor.getFoo().getGreenHopperConfig().toString());
        }
    }
}
