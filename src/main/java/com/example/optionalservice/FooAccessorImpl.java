package com.example.optionalservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Accessor implementation to safely get a {@link Foo} instance if one is available.
 */
public class FooAccessorImpl implements FooAccessor
{
    private static final Logger log = LoggerFactory.getLogger(FooAccessorImpl.class);

    private final ApplicationContext applicationContext;
    private Foo foo;

    public FooAccessorImpl(ApplicationContext applicationContext)
    {
        this.applicationContext = checkNotNull(applicationContext, "applicationContext");
    }

    @Override
    public synchronized Foo getFoo()
    {
        if (foo == null)
        {
            initFoo();
        }
        return foo;
    }

    private void initFoo()
    {
        try
        {
            Class<?> fooServiceFactoryClass = getFooServiceFactoryClass();
            if (fooServiceFactoryClass != null)
            {
                foo = ((FooServiceFactory)applicationContext.getAutowireCapableBeanFactory().
                    createBean(fooServiceFactoryClass, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false)).get();
            }
        }
        catch (Exception e)
        {
            log.debug("Could not create Foo", e);
        }
    }

    /**
     * If the necessary GreenHopper class is found on the classpath, GreenHopper exists.
     * In that case return the {@link FooServiceFactory} class, otherwise return null.
     */
    private Class<?> getFooServiceFactoryClass()
    {
        try
        {
            // Check to see if the class we depend on (from GreenHopper) is available
            getClass().getClassLoader().loadClass("com.pyxis.greenhopper.GreenHopper");
            // If we get to this point in the code, GreenHopper is available. Let's load the service factory
            return getClass().getClassLoader().loadClass("com.example.optionalservice.FooServiceFactory");
        }
        catch (Exception e)
        {
            log.info("The necessary GreenHopper class is unavailable.");
            return null;
        }
    }
}
